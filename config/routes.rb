Rails.application.routes.draw do
  resources :teachers
  resources :users
  resources :student_classrooms
  resources :students
  resources :classrooms
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end

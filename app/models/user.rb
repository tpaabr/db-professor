class User < ApplicationRecord
    has_one :teacher

    enum kind: {
        Student: "0",
        Teacher: "1"
    }
end

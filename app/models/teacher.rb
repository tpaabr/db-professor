class Teacher < ApplicationRecord
    has_many :classrooms
    belongs_to :user
end

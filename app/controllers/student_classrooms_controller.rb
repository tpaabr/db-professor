class StudentClassroomsController < ApplicationController
  before_action :set_student_classroom, only: [:show, :update, :destroy]

  # GET /student_classrooms
  def index
    @student_classrooms = StudentClassroom.all

    render json: @student_classrooms
  end

  # GET /student_classrooms/1
  def show
    render json: @student_classroom
  end

  # POST /student_classrooms
  def create
    @student_classroom = StudentClassroom.new(student_classroom_params)

    if @student_classroom.save
      render json: @student_classroom, status: :created, location: @student_classroom
    else
      render json: @student_classroom.errors, status: :unprocessable_entity
    end
  end

  # PATCH/PUT /student_classrooms/1
  def update
    if @student_classroom.update(student_classroom_params)
      render json: @student_classroom
    else
      render json: @student_classroom.errors, status: :unprocessable_entity
    end
  end

  # DELETE /student_classrooms/1
  def destroy
    @student_classroom.destroy
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_student_classroom
      @student_classroom = StudentClassroom.find(params[:id])
    end

    # Only allow a trusted parameter "white list" through.
    def student_classroom_params
      params.require(:student_classroom).permit(:classroom_id, :student_id)
    end
end

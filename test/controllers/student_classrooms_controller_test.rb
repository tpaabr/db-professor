require 'test_helper'

class StudentClassroomsControllerTest < ActionDispatch::IntegrationTest
  setup do
    @student_classroom = student_classrooms(:one)
  end

  test "should get index" do
    get student_classrooms_url, as: :json
    assert_response :success
  end

  test "should create student_classroom" do
    assert_difference('StudentClassroom.count') do
      post student_classrooms_url, params: { student_classroom: { classroom_id: @student_classroom.classroom_id, student_id: @student_classroom.student_id } }, as: :json
    end

    assert_response 201
  end

  test "should show student_classroom" do
    get student_classroom_url(@student_classroom), as: :json
    assert_response :success
  end

  test "should update student_classroom" do
    patch student_classroom_url(@student_classroom), params: { student_classroom: { classroom_id: @student_classroom.classroom_id, student_id: @student_classroom.student_id } }, as: :json
    assert_response 200
  end

  test "should destroy student_classroom" do
    assert_difference('StudentClassroom.count', -1) do
      delete student_classroom_url(@student_classroom), as: :json
    end

    assert_response 204
  end
end

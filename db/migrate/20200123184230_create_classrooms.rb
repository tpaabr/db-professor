class CreateClassrooms < ActiveRecord::Migration[5.1]
  def change
    create_table :classrooms do |t|
      t.string :nome
      t.string :codigo

      t.timestamps
    end
  end
end

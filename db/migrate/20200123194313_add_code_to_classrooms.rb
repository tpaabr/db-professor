class AddCodeToClassrooms < ActiveRecord::Migration[5.1]
  def change
    add_column :classrooms, :code, :string
  end
end

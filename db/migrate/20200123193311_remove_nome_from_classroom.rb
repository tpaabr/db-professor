class RemoveNomeFromClassroom < ActiveRecord::Migration[5.1]
  def change
    remove_column :classrooms, :nome, :string
  end
end
